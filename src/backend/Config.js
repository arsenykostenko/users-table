const fs = require('fs')
const path = require('path')

const _ = require('lodash')

const logger = require('./Logger.js')

const config = JSON.parse(fs.readFileSync(path.join(__dirname, '../../config.json'), 'utf8'))

const environment = process.env.ENVIRONMENT || ''

const environmentConfig = _.isEmpty(config[environment]) ? {} : config[environment]

const defaultConfig = config.default

const currentConfig = {}

_.assign(currentConfig, defaultConfig, environmentConfig)

config.currentConfig = currentConfig

logger.info('App config', config)

module.exports = config
