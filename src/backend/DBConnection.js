const { MongoClient } = require('mongodb')

const secrets = require('./Secrets.js')
const logger = require('./Logger.js')

const environment = process.env.ENVIRONMENT || ''
const dburlKey = environment ? `dburl-${environment}` : 'dburl'

module.exports = MongoClient.connect(secrets[dburlKey]).catch(dbError => {
    logger.error('Database connection error', dbError)
    process.exit(1)
}).then(db => {
    logger.info(`Connected correctly to ${environment} server`)
    return db
})
