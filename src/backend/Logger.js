const path = require('path')

const winston = require('winston')
const mkdirp = require('mkdirp')

mkdirp.sync(path.join(__dirname, '../../logs/'))

const logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            colorize: true,
            timestamp: true,
            prettyPrint: true,
            humanReadableUnhandledException: true,
            level: 'verbose',
        }),
        new (winston.transports.File)({
            level: 'verbose',
            timestamp: true,
            filename: path.join(__dirname, '../../logs/app.log'),
            maxsize: 1048576,
            maxFiles: 10,
            json: false,
            prettyPrint: true,
        }),
    ],
})

module.exports = logger
