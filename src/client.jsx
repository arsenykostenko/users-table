const $ = require('jquery')
const React = require('react')
const ReactDOM = require('react-dom')
const Immutable = require('immutable')
const { Provider } = require('react-redux')
const { createStore } = require('redux')
const { Router, Route, IndexRoute, browserHistory } = require('react-router')

const rootReducer = require('./redux/reducers.js')
const App = require('./components/App.jsx')
const Demo = require('./components/Demo.jsx')
const Error404 = require('./components/Error404.jsx')

if (window.parent !== window)
    window.__REACT_DEVTOOLS_GLOBAL_HOOK__ = window.parent.__REACT_DEVTOOLS_GLOBAL_HOOK__
window.$ = $

const state = {}

const initialState = Immutable.fromJS(state)

const store = createStore(
    rootReducer,
    initialState,
    window.devToolsExtension ? window.devToolsExtension() : f => f
)

const render = () => ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/" component={App}>
                <IndexRoute component={Demo} />
                <Route path="*" component={Error404} />
            </Route>
        </Router>
    </Provider>,
    document.getElementById('app-container')
)

$(() => render())
