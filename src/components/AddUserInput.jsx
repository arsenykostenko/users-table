const React = require('react')

const AddUserInput = React.createClass({
    propTypes: {
        onAddUser: React.PropTypes.func.isRequired,
    },

    getInitialState() {
        return {
            validationStatus: '',
            firstName: '',
            lastName: '',
            email: '',
        }
    },

    onFirstNameChange(e) {
        this.setState({ firstName: e.target.value })
    },

    onLastNameChange(e) {
        this.setState({ lastName: e.target.value })
    },

    onEmailChange(e) {
        this.setState({ email: e.target.value })
    },

    createNewUser() {
        const firstName = this.state.firstName || ''
        const lastName = this.state.lastName || ''
        const email = this.state.email || ''
        if (firstName.length === 0 || lastName.length === 0 || email.length === 0) {
            this.setState({ validationStatus: 'All fields are mandatory' })
            return
        }

        this.setState(this.getInitialState())
        this.props.onAddUser({ firstName, lastName, email })
    },

    render() {
        return (
            <div className="users-table-row users-table-row-add">
                <input className="users-table-column users-table-column-firstname"
                    placeholder="first name"
                    value={this.state.firstName}
                    onChange={this.onFirstNameChange}
                />
                <input className="users-table-column users-table-column-lastname"
                    placeholder="last name"
                    value={this.state.lastName}
                    onChange={this.onLastNameChange}
                />
                <input className="users-table-column users-table-column-email"
                    placeholder="email"
                    value={this.state.email}
                    onChange={this.onEmailChange}
                />
                <div className="users-table-column users-table-column-operations users-table-column-add"
                    onClick={() => this.createNewUser()}
                >&#10010;
                </div>
                <div className="users-table-input-validation">{this.state.validationStatus}</div>
            </div>
        )
    },
})

module.exports = AddUserInput
