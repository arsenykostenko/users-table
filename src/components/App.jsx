const React = require('react')
const Helmet = require('react-helmet')

require('../css/App.less')

const App = props => (
    <div>
        <Helmet
            defaultTitle="Test"
            titleTemplate="%s - Test"
        />
        <div id="content">
            {props.children}
        </div>
    </div>
)

App.propTypes = {
    children: React.PropTypes.any,
}

module.exports = App
