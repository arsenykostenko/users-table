const React = require('react')
const { connect } = require('react-redux')
const { Link } = require('react-router')
const $ = require('jquery')

const actions = require('../redux/actions.js')
const Loading = require('./Loading.jsx')
const UsersTable = require('./UsersTable.jsx')

require('../css/App.less')

const Demo = React.createClass({
    propTypes: {
        loaded: React.PropTypes.object.isRequired,
        users: React.PropTypes.object.isRequired,
        onAddUser: React.PropTypes.func.isRequired,
        onDeleteUser: React.PropTypes.func.isRequired,
        onFetchUsers: React.PropTypes.func.isRequired,
        onFetchUsersSuccess: React.PropTypes.func.isRequired,
        onFetchUsersFailure: React.PropTypes.func.isRequired,
    },

    componentDidMount() {
        this.props.onFetchUsers()
        $.ajax('/api/users')
            .fail(error => this.props.onFetchUsersFailure(error))
            .done(res => this.props.onFetchUsersSuccess(res.users))
    },

    render() {
        return (
            <div className="demo-container">
                <Loading msg="loading users" visible={!this.props.loaded.get('loaded')} />
                <div className="sources">
                    <Link to="https://bitbucket.org/arsenykostenko/users-table/src" target="_blank">
                        check out the source code
                    </Link>
                </div>
                <UsersTable
                    users={this.props.users}
                    onAddUser={this.props.onAddUser}
                    onDeleteUser={this.props.onDeleteUser}
                />
            </div>
        )
    },
})

const mapStateToProps = (state) => ({
    loaded: state.get('fetchUsers'),
    users: state.get('users'),
})

const mapDispatchToProps = (dispatch, getState) => ({
    onFetchUsers: () => {
        dispatch(actions.fetchUsers())
    },
    onFetchUsersSuccess: (users) => {
        dispatch(actions.fetchUsersSuccess(users))
    },
    onFetchUsersFailure: (error) => {
        dispatch(actions.fetchUsersFailure(error))
    },
    onDeleteUser: (user) => {
        $.ajax({
            url: `/api/user?${$.param({ idToDelete: user.get('_id') })}`,
            type: 'DELETE',
            contentType: 'application/json',
        })
        .done(res => {
            console.log('res', res)
            dispatch(actions.deleteUser(user))
        })
    },
    onAddUser: (user) => {
        $.post('/api/user', user)
            .fail(error => this.props.onFetchUsersFailure(error))
            .done(res => dispatch(actions.addUser(res.user)))
    },
})

module.exports = connect(
    mapStateToProps,
    mapDispatchToProps
)(Demo)
