const React = require('react')
const { Link } = require('react-router')

require('../css/Error.less')

const Error404 = (props) =>
(
    <div className="error-404">
        <Link to="/">go home</Link>
    </div>
)

Error404.propTypes = {
}

module.exports = Error404
