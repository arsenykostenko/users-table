const React = require('react')
const ReactCSSTransitionGroup = require('react-addons-css-transition-group')

require('../css/Loading.less')

const Loading = (props) => (
	<ReactCSSTransitionGroup transitionName="loading" transitionEnterTimeout={200} transitionLeaveTimeout={1300}>
        {props.visible ? <div className="loading">{props.msg}</div> : null}
    </ReactCSSTransitionGroup>
)

Loading.propTypes = {
    msg: React.PropTypes.string.isRequired,
    visible: React.PropTypes.bool.isRequired,
}

module.exports = Loading
