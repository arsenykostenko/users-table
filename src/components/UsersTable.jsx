const React = require('react')
const AddUserInput = require('./AddUserInput.jsx')

require('../css/UsersTable.less')

const UsersTable = React.createClass({
    propTypes: {
        users: React.PropTypes.object.isRequired,
        onDeleteUser: React.PropTypes.func.isRequired,
        onAddUser: React.PropTypes.func.isRequired,
    },

    getInitialState() {
        return {
            firstName: '',
            lastName: '',
            email: '',
        }
    },

    onFirstNameChange(e) {
        this.setState({ firstName: e.target.value })
    },

    onLastNameChange(e) {
        this.setState({ lastName: e.target.value })
    },

    onEmailChange(e) {
        this.setState({ email: e.target.value })
    },

    render() {
        return (
            <div className="users-table">
                <AddUserInput onAddUser={this.props.onAddUser} />
                <div className="users-table-row users-table-filter-row">
                    <input className="users-table-column users-table-column-firstname"
                        placeholder="first name"
                        onChange={this.onFirstNameChange}
                    />
                    <input className="users-table-column users-table-column-lastname"
                        placeholder="last name"
                        onChange={this.onLastNameChange}
                    />
                    <input className="users-table-column users-table-column-email"
                        placeholder="email"
                        onChange={this.onEmailChange}
                    />
                    <div className="users-table-column users-table-column-operations users-table-column-filter">
                    </div>
                </div>
                {
                    this.props.users
                    .filter(u =>
                        (this.state.firstName === '' ? true : u.get('firstName').includes(this.state.firstName))
                        &&
                        (this.state.lastName === '' ? true : u.get('lastName').includes(this.state.lastName))
                        &&
                        (this.state.email === '' ? true : u.get('email').includes(this.state.email))
                    )
                    .map((user, key) =>
                        <UserRow
                            key={`${key}-${user.get('email')}`}
                            user={user}
                            onDeleteUser={this.props.onDeleteUser}
                        />
                    )
                }
            </div>
        )
    },
})

const UserRow = (props) =>
(
    <div className="users-table-row">
        <div className="users-table-column users-table-column-firstname">{ props.user.get('firstName') }</div>
        <div className="users-table-column users-table-column-lastname">{ props.user.get('lastName') }</div>
        <div className="users-table-column users-table-column-email">{ props.user.get('email') }</div>
        <div className="users-table-column users-table-column-operations users-table-column-remove"
            onClick={() => props.onDeleteUser(props.user)}
        >&#10006;
        </div>
    </div>
)

UserRow.propTypes = {
    user: React.PropTypes.object.isRequired,
    onDeleteUser: React.PropTypes.func.isRequired,
}

module.exports = UsersTable
