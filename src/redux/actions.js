const ActionNames = require('./ActionNames.js')

module.exports = {
    addUser: (user) => ({
        type: ActionNames.ADD_USER,
        payload: {
            user,
        },
    }),

    deleteUser: (user) => ({
        type: ActionNames.DELETE_USER,
        payload: {
            user,
        },
    }),

    fetchUsers: () => ({ type: ActionNames.FETCH_USERS }),

    fetchUsersSuccess: (users) => ({
        type: ActionNames.FETCH_USERS_SUCCESS,
        payload: {
            users,
        },
    }),

    fetchUsersFailure: (error) => ({
        type: ActionNames.FETCH_USERS_FAILURE,
        payload: {
            error,
        },
    }),
}
