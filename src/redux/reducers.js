const Immutable = require('immutable')
const { combineReducers } = require('redux-immutable')
const ActionNames = require('./ActionNames.js')

const users = (state = Immutable.List(), action) => {
    switch (action.type) {
        case ActionNames.ADD_USER: {
            console.log(action.payload.user)
            return state.push(Immutable.fromJS(action.payload.user))
        }
        case ActionNames.DELETE_USER: {
            const i = state.findIndex(n => n.get('_id') === action.payload.user.get('_id'))
            return state.delete(i)
        }
        case ActionNames.FETCH_USERS_SUCCESS:
            return Immutable.fromJS(action.payload.users)
        default:
            return state
    }
}

const initialState = { loaded: false, loading: false, error: null }
const fetchUsers = (state = Immutable.fromJS(initialState), action) => {
    switch (action.type) {
        case ActionNames.FETCH_USERS:
            return state.set('loading', true).set('loaded', false).set('error', null)
        case ActionNames.FETCH_USERS_SUCCESS:
            return state.set('loading', false).set('loaded', true).set('error', null)
        case ActionNames.FETCH_USERS_FAILURE:
            return state.set('loading', false).set('loaded', false).set('error', action.payload.error)
        default:
            return state
    }
}

const rootReducer = combineReducers({
    users,
    fetchUsers,
})

module.exports = rootReducer
