const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const { ObjectID } = require('mongodb')
const http = require('http').Server(app)
const path = require('path')

const dbPromise = require('./backend/DBConnection.js')

dbPromise.then(db => {
    const users = db.collection('users')

    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(bodyParser.json())

    app.use('/static', express.static(path.join(__dirname, '../assets')))

    app.get('/api/users',
    (req, res) => {
        users
            .find()
            .sort([['firstName', 1]])
            .toArray()
            .then(u => {
                res.json({ users: u })
            })
            .catch(error => {
                res.json({ error })
            })
    })

    app.post('/api/user',
    (req, res) => {
        users
            .insertOne(req.body)
            .then(u => {
                res.json({ user: u.ops[0] })
            })
            .catch(error => {
                res.json({ error })
            })
    })

    app.delete('/api/user',
    (req, res) => {
        users
            .deleteOne({ _id: new ObjectID(req.query.idToDelete) })
            .then(u => {
                res.json({ deletedCount: u.deletedCount })
            })
            .catch(error => {
                res.json({ error })
            })
    })

    app.get('*', (req, res) => {
        res.sendFile(path.join(__dirname, '../assets/index.html'));
    })

    let port = Number(process.env.LISTEN_PORT)
    port = Number.isNaN(port) ? 3000 : port

    http.listen(port, () => {
        console.log(`Listening on *:${port}`)
    })

    http.on('close', () => {
        if (db)
            db.close()
    })
})
