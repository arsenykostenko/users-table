/*eslint-disable */
var webpack = require('webpack');
var path = require("path");

module.exports = {
	entry : [
        './src/client.jsx'
    ],
	output : {
        path: path.resolve(__dirname, 'assets'),
		filename : 'bundle.js'
	},
	module : {
		preLoaders: [
			{test: /\.jsx?$/, loader: "eslint-loader", exclude: /node_modules/}
		],
		loaders : [
			{
				test : /\.jsx?$/,
				exclude : /node_modules/,
				loaders : ['babel']
			},
			{
				test: /\.less$/,
				loader: "style!raw!less"
			},
			{
				test: /\.json$/,
				loader: "json"
			},
			{
				test: /\.(jpg|png)$/,
				loader: 'url'
			},
			{
				test: /\.css$/,
				loader: 'style!css!'
			}
		]
	},
	resolve : {
		extensions : ['','.js','.jsx']
	},
    eslint: {
        configFile: '.eslintrc.json'
    }
}